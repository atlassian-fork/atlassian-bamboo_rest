require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_user_repositories) do
  @doc = 'Puppet type for bamboo user repositories'

  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. concurrent).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:type) do
    desc 'Type of user repository, can be Crowd, Custom, Local'

    validate do |value|
      raise ArgumentError, "Type must be specified" if value.nil?
      raise ArgumentError, "Type must be of type Crowd, Local or Custom" unless value == "Crowd" || value == "Local" || value == "Custom"
    end
  end

  newproperty(:server_url) do
    desc 'The server url of the Crowd/Jira server. For example, http//jira.example.com:8080/ or http://example.org:8095/crowd (Only used if type is Crowd).'
    validate do |value|
      raise ArgumentError, "Server url can not be an empty string" if value.empty?
    end
  end

  newproperty(:application_name) do
    desc 'Bamboo authenticates with JIRA or Crowd using this name. (only used if type is Crowd)'

    validate do |value|
      raise ArgumentError, "Application name can not be an empty string" if value.empty?
    end
  end

  newproperty(:application_password) do
    desc 'Bamboo authenticates with JIRA or Crowd using this password. (only used if type is Crowd)'

    validate do |value|
      raise ArgumentError, "Application password can not be an empty string" if value.empty?
    end

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return "A password has been created"
      else
        return "The password has been changed"
      end
    end

    def is_to_s( currentvalue )
      return '[old password hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new password hash redacted]'
    end
  end

  newproperty(:cache_refresh_interval) do
    desc 'Time (in minutes) between updating the list of users and groups. (only used if type is Crowd)'

    validate do |value|
      raise ArgumentError, "Cache refresh interval be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Cache refresh interval must bigger than or equal to one" unless value.to_i >= 1
    end

    munge { |value| Integer(value) }
  end

  validate do
    raise ArgumentError, "Server url can only be set if type is Crowd" if self[:type] != 'Crowd' and (!self[:server_url].nil?)
    raise ArgumentError, "Application name can only be set if type is Crowd" if self[:type] != 'Crowd' and (!self[:application_name].nil?)
    raise ArgumentError, "Application password can only be set if type is Crowd" if self[:type] != 'Crowd' and (!self[:application_password].nil?)
    raise ArgumentError, "Cache refresh interval can only be set if type is Crowd" if self[:type] != 'Crowd' and (!self[:cache_refresh_interval].nil?)
  end
end
