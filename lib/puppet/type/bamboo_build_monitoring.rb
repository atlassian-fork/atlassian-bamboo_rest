require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_build_monitoring) do
  @doc = 'Puppet type for bamboo build monitoring'

  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. monitoring).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:build_monitoring_enabled, :boolean => true) do
    desc 'Whether build monitoring is enabled'

    newvalue(:true)
    newvalue(:false)
  end


  newproperty(:build_queue_minutes_timeout_default) do
    desc 'Default timeout for build queues'

    validate do |value|
      raise ArgumentError, "Default queue timeout must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Default queue timeout must bigger than or equal to one" unless value.to_i >= 1
    end

    munge { |value| Integer(value) }
  end

  newproperty(:log_quiet_minutes_time_default) do
    desc 'Default timeout for logs'

    validate do |value|
      raise ArgumentError, "Default log timeout must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Default log timeout must bigger than or equal to one" unless value.to_i >= 1
    end

    munge { |value| Integer(value) }
  end

  newproperty(:build_time_multiplier_default) do
    desc 'Default multiplier for build times'

    validate do |value|
      raise ArgumentError, "Default multiplier must be a non-negative number, got #{value}" unless value.to_s =~ /\d+(\.\d+)?/
      raise ArgumentError, "Default log timeout must bigger than or equal to one" unless value.to_f > 0.0
    end

    munge { |value| Float(value) }
  end
end