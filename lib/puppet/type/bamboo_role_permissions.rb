Puppet::Type.newtype(:bamboo_role_permissions) do
  @doc = 'Puppet type to manage role permissions in bamboo. Only support 2 roles: ROLE_USER and ROLE_ANONYMOUS'

  newparam(:name, :namevar => true) do
    desc 'name of the role, e.g. ROLE_USER, ROLE_ANONYMOUS'
    newvalues(:ROLE_USER, :ROLE_ANONYMOUS)
  end

  newproperty(:permissions, :array_matching => :all) do
    desc 'list of permissions of this role'

    validate do |value|
      raise ArgumentError, 'Valid permission can only contain ACCESS, SOX_COMPLIANCE and CREATE_PLAN' unless value =~ /\A(ACCESS|SOX_COMPLIANCE|CREATE_PLAN|CREATE_REPOSITORY)\z/
    end

    def insync?(is)
      return false unless is.length == @should.length
      is & @should == is
    end
  end

  validate do
    raise ArgumentError, 'Anonymous user cannot have CREATE_PLAN permission' if self['name'] == :ROLE_ANONYMOUS and self['permissions'].include?('CREATE_PLAN')
    raise ArgumentError, 'Anonymous user cannot have SOX_COMPLIANCE permission' if self['name'] == :ROLE_ANONYMOUS and self['permissions'].include?('SOX_COMPLIANCE')
    raise ArgumentError, 'Anonymous user cannot have CREATE_REPOSITORY permission' if self['name'] == :ROLE_ANONYMOUS and self['permissions'].include?('CREATE_REPOSITORY')
  end
end