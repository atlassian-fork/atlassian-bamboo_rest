require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_im_server) do
  @doc = 'Puppet type to manage bamboo im server configuration'

  ensurable

  newparam(:name, :namevar => true) do
    desc 'name of the resource, must be current because there can be only one im server configured in bamboo'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:host) do
    desc 'Host name of IM server'

    validate do |value|
      raise ArgumentError, 'host cannot be empty' if value.empty?
    end
  end

  newproperty(:port) do
    desc 'Port of IM server'

    validate do |value|
      raise ArgumentError, 'IM server_port can only be positive integer' unless value.to_s =~ /\A\d+\z/
    end

    munge { |value| Integer(value) }
  end

  newproperty(:username) do
    desc 'Username for the IM server'
  end

  newproperty(:password) do
    desc 'password for the IM Server'

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return 'IM server password has been created'
      else
        return 'IM server password has been changed'
      end
    end

    def is_to_s( currentvalue )
      return '[old IM server password hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new IM server password hash redacted]'
    end

  end

  newproperty(:resource_name) do
    desc 'Name of resource used to distinguish connections if multiple clients connect to the same Jabber account'
  end

  newproperty(:tls_enabled) do
    desc 'Whether a TLS/SSL connection is required to the IM Server'

    newvalues(:true, :false)
  end

  validate do
    raise ArgumentError, 'Host must be specified' if self[:host].nil? && self[:ensure] == :present
  end

end