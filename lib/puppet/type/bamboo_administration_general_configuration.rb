require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_administration_general_configuration) do
  @doc = "Puppet type for bamboo admin general configuration"

  newparam(:name, :namevar => true) do
    desc 'Name of the configuration (i.e. current).'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  def munge_boolean(value)
    return :true if [true, "true", :true].include? value
    return :false if [false, "false", :false].include? value
    fail("Expect boolean parameter, got '#{value}'")
  end

  newproperty(:base_url) do
    desc 'This is the base URL of this installation of Bamboo. All links created (for emails etc) will be prefixed by This URL.'

    munge {|value| value.chomp("/")}
  end

  newproperty(:instance_name) do
    desc 'Instance name of bamboo server'
  end

  newproperty(:broker_url) do
    desc 'Broker url'
  end

  newproperty(:broker_client_url) do
    desc 'Broker client url'
  end

  newproperty(:dashboard_page_size) do
    desc 'Number of plans to initially show on the dashboard'
    validate do |value|
      raise ArgumentError, "Dashboard page size must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Dashboard page size must bigger than or equal to one" unless value.to_i >= 1
    end
    munge { |value| Integer(value) }
  end

  newproperty(:branch_detection_interval) do
    desc 'How often to check for new plan branches (seconds)'
    validate do |value|
      raise ArgumentError, "Branch detection interval must be a non-negative integer, got #{value}" unless value.to_s =~ /\d+/
      raise ArgumentError, "Branch detection interval must bigger than or equal to one" unless value.to_i >= 1
    end
    munge { |value| Integer(value) }
  end

  newproperty(:gravatar_enabled, :boolean => true) do
    desc 'Gravatar support allows Bamboo to use user email addresses to retrieve profile pictures from Gravatar or a Gravatar enabled service. Allowed value: true/false'

      newvalue(:true)
      newvalue(:false)

    munge do |value|
      @resource.munge_boolean(value)
    end
  end

  newproperty(:gravatar_server_url) do
    desc 'URL of the external Gravatar server'

    munge {|value| value.chomp("/")}
  end

  newproperty(:gzip_compression_enabled, :boolean => true) do
    desc 'This is useful if Bamboo is being run over slow networks. There is a slight performance penalty, and may not work for non-english languages.'

    newvalue(:true)
    newvalue(:false)

    munge do |value|
      @resource.munge_boolean(value)
    end
  end
end