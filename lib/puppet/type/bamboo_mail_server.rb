require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'hash_password.rb'))
require File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'puppet_x', 'bamboo', 'util.rb'))

Puppet::Type.newtype(:bamboo_mail_server) do
  @doc = 'Puppet type to manage mail server settings in bamboo'

  newparam(:name, :namevar => true) do
    desc 'name of the puppet resource, can only be "current"'

    validate do |value|
      Util.assert_equal_to_current(value)
    end
  end

  newproperty(:from) do
    desc 'Sender of the email'

    validate do |value|
      raise ArgumentError, 'from cannot be empty' if value.to_s.empty?
    end
  end

  newproperty(:from_address) do
    desc 'From address of the email'

    validate do |value|
      raise ArgumentError, 'Invalid email address' unless value.to_s =~ /@/
    end
  end

  newproperty(:subject_prefix) do
    desc 'This is the tag added to the start of the subject line to identify Bamboo-generated email. e.g. [Bamboo].'
  end

  newproperty(:precedence_bulk_header_excluded) do
    desc 'Whether to prevent the Precedence: Bulk header on bamboo notification emails'

    newvalues(:true, :false)
  end

  newproperty(:email_settings) do
    desc 'Email provider type, e.g. SMTP or JNDI'

    newvalues(:SMTP, :JNDI)
  end

  newproperty(:smtp_server) do
    desc 'The email server used to send out the build results. For example "mail.myserver.com".'

    validate do |value|
      raise ArgumentError, 'smtp_server cannot be empty' if value.to_s.empty?
    end
  end

  newproperty(:smtp_port) do
    desc 'SMTP server port'

    validate do |value|
      raise ArgumentError, 'smtp_port can only be positive integer' unless value.to_s =~ /\A\d+\z/
    end

    munge { |value| Integer(value) }
  end

  newproperty(:smtp_username) do
    desc 'Username used to connect to SMPT server'
  end

  newproperty(:smtp_password) do
    desc 'Password used to connect to SMTP server'

    # Changes the comparison function
    def insync?(is)
      if(is == :absent and @should[0] != :absent)
        return false
      elsif(is != :absent and @should[0] == :absent)
        return false
      elsif(is == :absent and @should[0] == :absent)
        return true
      else
        Bamboo::HashPassword.check_password_hash(is, @should[0])
      end
    end

    #Log messages are changed so that the password does not appear in the logs.
    def change_to_s(currentvalue, newvalue)
      if currentvalue == :absent
        return 'smtp password has been created'
      else
        return 'smtp password has been changed'
      end
    end

    def is_to_s( currentvalue )
      return '[old smtp password hash redacted]'
    end

    def should_to_s( newvalue )
      return '[new smtp password hash redacted]'
    end
  end

  newproperty(:tls_enabled) do
    desc 'Whether Transport Layer Security (TLS) is enabled'

    newvalues(:true, :false)
  end

  newproperty(:jndi_location) do
    desc 'JNDI name of your SMTP server configuration in your application server'

    validate do |value|
      raise ArgumentError, 'jndi_location cannot be empty' if value.to_s.empty?
    end
  end

  validate do
    raise ArgumentError, 'SMTP server and port cannot be empty' if self['email_settings'] == :SMTP && (self['smtp_server'].nil? || self['smtp_port'].nil?)
    raise ArgumentError, 'jndi_location should not be set when email_settings is SMTP' if self['email_settings'] == :SMTP && !self['jndi_location'].nil?
    raise ArgumentError, 'JNDI name cannot be empty' if self['email_settings'] == :JNDI && self['jndi_location'].nil?
    if self['email_settings'] == :JNDI && (!self['smtp_server'].nil? || !self['smtp_port'].nil? || !self['smtp_username'].nil? || !self['smtp_password'].nil? || !self['tls_enabled'].nil?)
      raise ArgumentError, 'smtp_server, smtp_port, smtp_username, smtp_password and tls_enabled should not be set when email_settings is SMTP'
    end
  end
end