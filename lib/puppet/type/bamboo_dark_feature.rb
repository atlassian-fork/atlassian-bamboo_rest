Puppet::Type.newtype(:bamboo_dark_feature) do

  @doc = 'Puppet type to manage Bamboo dynamic dark features'

  ensurable

  newparam(:name) do
    desc 'name of the resource, which maps to the dark feature name (e.g. "bamboo.final.stages")'

    validate do |value|
      raise ArgumentError, 'name cannot be empty' if value.to_s.empty?
    end
  end

end
