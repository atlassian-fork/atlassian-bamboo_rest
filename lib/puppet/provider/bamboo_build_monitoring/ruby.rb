require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_build_monitoring).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc 'Puppet provider to manage bamboo build monitoring configuration'

  # REST endpoint to manage bamboo build concurrency
  def self.bamboo_resource
    '/rest/admin/latest/config/build/monitoring'
  end

  def self.map_config_to_resource_hash(build_monitoring)
    {
      :build_monitoring_enabled             => build_monitoring['buildMonitoringEnabled'].to_s.intern,
      :build_time_multiplier_default        => build_monitoring['buildTimeMultiplierDefault'],
      :log_quiet_minutes_time_default       => build_monitoring['logQuietMinutesTimeDefault'],
      :build_queue_minutes_timeout_default  => build_monitoring['buildQueueMinutesTimeoutDefault']
    }
  end

  def map_resource_hash_to_config
    config = {}

    config['buildMonitoringEnabled'] = resource[:build_monitoring_enabled] unless resource[:build_monitoring_enabled].nil?
    config['buildTimeMultiplierDefault'] = resource[:build_time_multiplier_default] unless resource[:build_time_multiplier_default].nil?
    config['logQuietMinutesTimeDefault'] = resource[:log_quiet_minutes_time_default] unless resource[:log_quiet_minutes_time_default].nil?
    config['buildQueueMinutesTimeoutDefault'] = resource[:build_queue_minutes_timeout_default] unless resource[:build_queue_minutes_timeout_default].nil?

    config
  end

  # Ask puppet to handle attribute getter/setter methods
  mk_resource_methods

end
