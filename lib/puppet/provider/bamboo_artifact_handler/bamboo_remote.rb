require 'json'
require File.expand_path(File.join(File.dirname(__FILE__), 'bamboo_artifact_handler_provider.rb'))

Puppet::Type.type(:bamboo_artifact_handler).provide(:bamboo_remote, :parent => Puppet::Provider::BambooArtifactHandlerProvider) do
  desc 'Puppet provider for type bamboo remote artifact handler'

  confine :true => begin
            name.to_s.intern == :bamboo_remote
          end

  def self.bamboo_resource
    '/rest/admin/latest/artifactHandlers/bambooRemote'
  end

  def self.artifact_handler_type
    'bamboo_remote'
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end