require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_audit_log).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc "Puppet provider to enable/disable bamboo audit log"

  #REST endpoint to manage audit log
  def self.bamboo_resource
    '/rest/admin/latest/config/auditLog'
  end

  # Map json result from REST endpoint to resource hash
  def self.map_config_to_resource_hash(audit_log_config)
    {
        :audit_log_enabled => audit_log_config['auditLoggingEnabled'].to_s.intern,
    }
  end

  # map resource configuration to json that accepted by bamboo REST endpoint
  def map_resource_hash_to_config
    config = {}
    config['auditLoggingEnabled'] = resource[:audit_log_enabled] unless resource[:audit_log_enabled].nil?

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
