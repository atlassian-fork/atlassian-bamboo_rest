require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_provider.rb'))

Puppet::Type.type(:bamboo_remote_agent_support).provide(:ruby, :parent => Puppet::Provider::BambooProvider) do
  desc "Puppet provider to enable/disable bamboo remote agent support"

  #REST endpoint to manage bamboo remote agent support
  def self.bamboo_resource
    '/rest/admin/latest/config/remoteAgentSupport'
  end

  # Map json result from REST endpoint to resource hash
  def self.map_config_to_resource_hash(remote_agent_support_config)
    {
      :remote_agents_supported => remote_agent_support_config['remoteAgentsSupported'].to_s.intern,
    }
  end

  # map resource configuration to json that accepted by bamboo REST endpoint
  def map_resource_hash_to_config
    config = {}
    config['remoteAgentsSupported'] = resource[:remote_agents_supported] unless resource[:remote_agents_supported].nil?

    config
  end

  # ask puppet to create the get/set methods automatically
  mk_resource_methods

end
