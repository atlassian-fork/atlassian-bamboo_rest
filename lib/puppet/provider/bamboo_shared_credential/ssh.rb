require File.expand_path(File.join(File.dirname(__FILE__), '..', 'bamboo_ensurable_provider.rb'))

Puppet::Type.type(:bamboo_shared_credential).provide(:ssh, :parent => Puppet::Provider::BambooEnsurableProvider) do
  @doc = 'Puppet type to manage bamboo shared credential'

  def self.bamboo_resource
    '/rest/admin/latest/config/sharedCredentials/ssh/'
  end

  def self.map_config_to_resource_hash(response)
    shared_creds = response['results']

    shared_creds.collect do |shared_cred|
      new(
        {
          :ensure         => :present,
          :name           => shared_cred['name'],
          :ssh_key        => shared_cred['attributes']['sshKey'],
          :ssh_passphrase => shared_cred['attributes']['sshPassphrase'],
          :id             => shared_cred['id'],
        }
      )
    end
  end

  # map resource hash to format accepted by REST resource
  def map_resource_hash_to_config
    raise ArgumentError, 'ssh_key cannot be nil' if resource[:ssh_key].nil?

    config = {}
    config['name'] = resource[:name]
    ssh_config = {}
    ssh_config['sshKey'] = resource[:ssh_key]
    ssh_config['sshPassphrase'] = resource[:ssh_passphrase] unless resource[:ssh_passphrase].nil?
    config['attributes'] = ssh_config
    config
  end

  mk_resource_methods

  def ssh_key=(val)
    @dirty_flag = true;
  end

  def ssh_passphrase=(val)
    @dirty_flag = true;
  end

  def name=(val)
    @dirty_flag = true
  end

end