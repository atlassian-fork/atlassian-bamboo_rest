require 'json'
require 'yaml'
require 'net/http'
require 'uri'

require File.expand_path(File.join(File.dirname(__FILE__),  'config.rb'))
require File.expand_path(File.join(File.dirname(__FILE__),  'server.rb'))
require File.expand_path(File.join(File.dirname(__FILE__),  'exception.rb'))

module Bamboo
  # The class to invoke bamboo REST endpoint to GET/PUT configurations
  class Rest
    attr_reader :bamboo_host
    attr_reader :bamboo_port

    DEFAULT_BAMBOO_HOST = 'localhost'
    DEFAULT_BAMBOO_PORT = '8085'
    DEFAULT_BAMBOO_BASE_URL = DEFAULT_BAMBOO_HOST + ":" + DEFAULT_BAMBOO_PORT

    def initialize(options, bamboo_base_url = DEFAULT_BAMBOO_BASE_URL)
      # https://docs.ruby-lang.org/en/2.1.0/URI.
      components = URI.split(bamboo_base_url)
      @bamboo_host  = components[2]
      port = components[3]
      if port.nil?
        @bamboo_port = DEFAULT_BAMBOO_PORT
      else
        @bamboo_port = port
      end
      @retries      = 10
      @user         = options[:admin_username]
      @password     = options[:admin_password]
      @timeout      = options[:connection_timeout]
      @open_timeout = options[:connection_open_timeout]
    end

    def self.with_client
      Bamboo::Config.configure { |bamboo_base_url, options|
        yield Rest.new(options, bamboo_base_url)
      }
    end

    def make_request(path, method)
      Net::HTTP.start(@bamboo_host, @bamboo_port) do |http|
        case method
          when 'get'
            req = Net::HTTP::Get.new(path)
          when 'put'
            req = Net::HTTP::Put.new(path)
          when 'post'
            req = Net::HTTP::Post.new(path)
          when 'delete'
            req = Net::HTTP::Delete.new(path)
          else
            raise "Unsupported HTTP method: #{method} for path #{path}"
        end

        req.basic_auth @user, @password
        http.read_timeout=@timeout
        http.open_timeout=@open_timeout

        # Required to avoid XSS alerts
        req['X-Atlassian-Token'] = 'no-check'
        # Fill the hole
        yield req
        http.request req
      end
    end

    def self.service
      @service = init_service
    end

    def self.init_service
      Bamboo::Config.configure { |bamboo_base_url, options|
        client = Bamboo::Service::HealthCheckClient.new(options)
        service = Bamboo::Service.new(client, options)
        Bamboo::CachingService.new(service)
      }
    end

    # Retrieve configurations of bamboo via REST endpoint
    # Make sure bamboo is running before actually invoking the endpoint
    def self.get_bamboo_settings(resource_name, content_type='application/json')
      service.ensure_running
      with_client do |client|
        response = client.make_request(resource_name, 'get') do |req|
          req['Accept'] = content_type
        end

        if response.is_a?(Net::HTTPSuccess)
          begin
            JSON.parse(response.body)
          rescue => e
            raise "Could not parse the JSON response from Bamboo (url: #{client.bamboo_host}, resource: #{resource_name}): #{e} (response: #{response})"
          end
        else
          yield response if block_given?
          Bamboo::ExceptionHandler.process(response) { |msg|
            raise "Could not get #{resource_name}, #{msg} "
          }
        end
      end
    end

    # Update bamboo configurations via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.update_bamboo_settings(resource_name, config, method = 'put', content_type='application/json')
      if method != 'put' && method != 'post'
        raise 'Invalid method given'
      end
      service.ensure_running
      with_client do |client|
        response = client.make_request(resource_name, method) do |req|
          req['Content-Type'] = content_type
          req['Accept'] = content_type
          req.body = JSON.generate(config)
        end
        if !response.is_a? Net::HTTPSuccess
          Bamboo::ExceptionHandler.process(response) do |msg|
            raise "Could not update #{resource_name} at #{client.bamboo_host}: #{msg}"
          end
        end
      end
    end

    # Create a bamboo resource via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.create_bamboo_resource(resource_name, config)
      service.ensure_running
      with_client do |client|
        response = client.make_request(resource_name, 'post') do |req|
          req['Content-Type'] = 'application/json'
          req['Accept'] = 'application/json'
          req.body = JSON.generate(config)
        end
        if !response.is_a?(Net::HTTPSuccess)
          Bamboo::ExceptionHandler.process(response) { |msg|
            raise "Could not create #{resource_name} at #{client.bamboo_host}: #{msg}"
          }
        end
      end
    end

    # Delete bamboo configurations via REST end point
    # Make sure bamboo is running before actually invoking the endpoint
    def self.delete_bamboo_resource(resource_name)
      service.ensure_running
      with_client { |client|
        response = client.make_request(resource_name, 'delete') { |req|
          req['Accept'] = 'application/json'
        }

        if response.is_a?(Net::HTTPNotFound)
          # Do nothing here, the resource has already been deleted
        elsif response.is_a?(Net::HTTPSuccess)
          # Good response
        else
          Bamboo::ExceptionHandler.process(response) { |msg|
            raise "Could not delete #{resource_name} from #{client.bamboo_host}: #{msg}"
          }
        end
      }
    end
  end
end
