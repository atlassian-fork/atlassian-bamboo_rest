require 'puppet_x/bamboo/server'
require 'spec_helper'

describe Bamboo::Service do
  let(:configuration) do
    {
      :bamboo_base_url      => 'http://example.com',
      :admin_username       => 'foobar',
      :admin_password       => 'secret',
      :health_check_retries => 3,
      :health_check_timeout => 0,
    }
  end

  describe 'HealthCheckClient.check_health' do
    let(:client) { Bamboo::Service::HealthCheckClient.new(configuration) }

    specify 'should talk to /rest/api/latest/info' do
      stub = stub_request(:any, /rest\/api\/latest\/info/)
      client.check_health
      expect(stub).to have_been_requested
    end

    specify 'should report running if the instance is up' do
      stub_request(:any, /.*/).to_return(:status => 200)
      expect(client.check_health.status).to eq(:running)
    end

    specify 'should report broken if the instance failed to start up' do
      stub_request(:any, /.*/).to_return(:status => 500)
      expect(client.check_health.status).to eq(:broken)
    end

    specify 'should report not running if the instance is still starting up' do
      stub_request(:any, /.*/).to_return(:status => 404)
      expect(client.check_health.status).to eq(:not_running)
      expect(client.check_health.log_message).to match(/Bamboo service is not running, yet./)
    end
  end

  describe 'CachingService.ensure_running' do
    let(:delegatee) { double('The real service') }
    let(:service) { Bamboo::CachingService.new(delegatee) }

    specify 'should delegate to the real service' do
      expect(delegatee).to receive(:ensure_running)
      expect { service.ensure_running }.to_not raise_error
    end

    specify 'should not cache successful result' do
      expect(delegatee).to receive(:ensure_running).exactly(2).times
      service.ensure_running
      expect { service.ensure_running }.to_not raise_error
    end

    specify 'should cache a negative result' do
      expect(delegatee).to receive(:ensure_running).exactly(1).times.and_raise('service is borked')
      expect { service.ensure_running }.to raise_error(RuntimeError, /service is borked/)
      expect { service.ensure_running }.to raise_error(RuntimeError, /Bamboo service failed a previous health check/)
    end
  end

  describe :ensure_running do
    let(:client) { double('Dummy Health Check Client') }
    let(:service) { Bamboo::Service.new(client, configuration) }

    specify 'should retry if service is not running' do
      expect(client).to receive(:check_health).and_return(Bamboo::Service::Status.not_running('still starting'), Bamboo::Service::Status.running)
      expect { service.ensure_running }.to_not raise_error
    end

    specify 'should bail out immediately if the service is broken' do
      expect(client).to receive(:check_health).and_return(Bamboo::Service::Status.broken('service is borked'))
      expect { service.ensure_running }.to raise_error(RuntimeError, /service is borked/)
    end

    specify "should retry no more than configured" do
      expect(client).to receive(:check_health).at_most(configuration[:health_check_retries]).times.and_return(Bamboo::Service::Status.not_running('still starting'))
      expect { service.ensure_running }.to raise_error(RuntimeError, /Bamboo service did not start up within 0 seconds/)
    end
  end
end
