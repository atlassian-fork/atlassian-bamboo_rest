require 'spec_helper'

describe Puppet::Type.type(:bamboo_mail_server) do
  describe '#name' do
    specify 'should accept value current' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'SMTP', :smtp_server => 'mail.a.c', :smtp_port => 22)}.to_not raise_error
    end

    specify 'should not accept value other than current' do
      expect{described_class.new(:name => 'bamboo-mail', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'SMTP', :smtp_server => 'mail.a.c', :smtp_port => 22)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#from' do
    specify 'should not be empty' do
      expect{described_class.new(:name => 'current', :from => '', :from_address => 'bamboo@atlassan.com', :email_settings => 'SMTP', :smtp_server => 'mail.a.c', :smtp_port => 22)}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#from_address' do
    specify 'should accept valid email address' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'JNDI', :jndi_location => 'mailserver')}.to_not raise_error
    end

    specify 'should not accept invalid email address' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'atlassian.com', :email_settings => 'JNDI', :jndi_location => 'mailserver')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#email_settings' do
    specify 'cannot accept values other than JNDI and SMTP' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'test', :jndi_location => 'mailserver')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#smtp_server' do
    specify 'cannot be nil if email_settings is SMTP' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'SMTP', :smtp_port => 25)}.to raise_error(Puppet::ResourceError)
    end

    specify 'cannot be empty if email_settings is SMTP' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'SMTP', :smtp_server => '', :smtp_port => 25)}.to raise_error(Puppet::ResourceError)
    end

    specify 'can be nil if email_settings is JNDI' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'JNDI', :jndi_location => 'mailserver')}.to_not raise_error
    end
  end

  describe '#server_port' do
    specify 'cannot be alphabetic' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'SMTP', :smtp_server => 'atlassian.com', :smtp_port => 'abc')}.to raise_error(Puppet::ResourceError)
    end

    specify 'cannot be negative integer' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'SMTP', :smtp_server => 'atlassian.com', :smtp_port => '-22')}.to raise_error(Puppet::ResourceError)
    end

    specify 'can not be nil if email_settings is SMTP' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'SMTP', :smtp_server => 'atlassian.com')}.to raise_error(Puppet::ResourceError)
    end

    specify 'can only be positive integer' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'SMTP', :smtp_server => 'atlassian.com', :smtp_port => '22')}.to_not raise_error
    end
  end

  describe '#jndi_location' do
    specify 'cannot be nil if mail_settings is JNDI' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'JNDI')}.to raise_error(Puppet::ResourceError)
    end

    specify 'cannot be empty if mail_settings is JNDI' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'JNDI', :jndi_location => '')}.to raise_error(Puppet::ResourceError)
    end

    specify 'can be nil if mail_settings is SMTP' do
      expect {described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian.com', :email_settings => 'SMTP', :smtp_port => 22, :smtp_server => 'atlassian.com')}.to_not raise_error
    end
  end

  describe '#validate' do
    specify 'jndi_location should be nil if email_settings is smtp' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'SMTP', :smtp_server => 'atlassian.com', :smtp_port => '22', :jndi_location=>'mail')}.to raise_error(Puppet::ResourceError)
    end

    specify 'smtp attributes should be nil if email_settings is jndi' do
      expect{described_class.new(:name => 'current', :from => 'bamboo', :from_address => 'bamboo@atlassian', :email_settings => 'JNDI', :smtp_server => 'atlassian.com', :smtp_port => '22', :jndi_location=>'mail')}.to raise_error(Puppet::ResourceError)
    end
  end
end
