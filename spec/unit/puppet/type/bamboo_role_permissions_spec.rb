require 'spec_helper'

describe Puppet::Type.type(:bamboo_role_permissions) do
  describe '#name' do
    specify 'should allow ROLE_USER' do
       expect {described_class.new(:name => :ROLE_USER, :permissions => ['ACCESS'])}.to_not raise_error
    end

    specify 'should allow ROLE_ANONYMOUS' do
      expect {described_class.new(:name => :ROLE_ANONYMOUS, :permissions => ['ACCESS'])}.to_not raise_error
    end

    specify 'should not allow invalid role name' do
      expect { described_class.new(:name => :TEST, :permissions => ['ACCESS'])}.to raise_error(Puppet::ResourceError)
    end
  end

  describe ':permissions' do
    specify 'should allow CREATE_PLAN, CREATE_REPOSITORY, SOX_COMPLIANCE and ACCESS permissions' do
      expect { described_class.new(:name => :ROLE_USER, :permissions => ['ACCESS', 'SOX_COMPLIANCE', 'CREATE_PLAN', 'CREATE_REPOSITORY'])}.to_not raise_error
    end

    specify 'should not allow invalid permissions' do
      expect { described_class.new(:name => :ROLE_USER, :permissions => ['ACCESS', 'RESTRICTED_ADMIN'])}.to raise_error(Puppet::ResourceError)
    end

    specify 'should calcuate permissions equality correctly' do
      expect(described_class.new(:name => :ROLE_USER, :permissions => ['ACCESS', 'SOX_COMPLIANCE', 'CREATE_PLAN']).property(:permissions).insync?(['CREATE_PLAN', 'SOX_COMPLIANCE', 'ACCESS'])).to eq(true)
    end

    specify 'should calculate permissions inequality correctly' do
      expect(described_class.new(:name => :ROLE_USER, :permissions => ['ACCESS', 'CREATE_PLAN']).property(:permissions).insync?(['CREATE_PLAN'])).to eq(false)
    end

    specify 'should only allow ACCESS permission for anonymous users' do
      expect {described_class.new(:name => :ROLE_ANONYMOUS, :permissions => ['ACCESS'])}.to_not raise_error
    end

    specify 'should not allow SOX_COMPLIANCE permission for anonymous users' do
      expect {described_class.new(:name => :ROLE_ANONYMOUS, :permissions => ['SOX_COMPLIANCE'])}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not allow CREATE_PLAN permission for anonymous users' do
      expect {described_class.new(:name => :ROLE_ANONYMOUS, :permissions => ['CREATE_PLAN'])}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not allow CREATE_REPOSITORY permission for anonymous users' do
      expect {described_class.new(:name => :ROLE_ANONYMOUS, :permissions => ['CREATE_REPOSITORY'])}.to raise_error(Puppet::ResourceError)
    end
  end
end
