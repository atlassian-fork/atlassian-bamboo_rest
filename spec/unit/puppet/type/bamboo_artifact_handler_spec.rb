require 'spec_helper'

describe Puppet::Type::type(:bamboo_artifact_handler) do

  describe '#name' do
    specify 'should accept server_local' do
      expect{described_class.new(:name => 'server_local')}.to_not raise_error
    end

    specify 'should accept agent_local' do
      expect{described_class.new(:name => 'agent_local')}.to_not raise_error
    end

    specify 'should accept bamboo_remote' do
      expect{described_class.new(:name => 'bamboo_remote')}.to_not raise_error
    end

    specify 'should accept s3' do
      expect{described_class.new(:name =>'s3')}.to_not raise_error
    end

    specify 'should not accept current' do
      expect{described_class.new(:name => 'current')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'shared_artifact_handler_enabled' do
    specify 'should accept boolean value' do
      expect {described_class.new(:name => 'server_local', :shared_artifacts_enabled => 'true')}.to_not raise_error
    end

    specify 'should not accept non-boolean value' do
      expect {described_class.new(:name => 'server_local', :shared_artifacts_enabled => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'non_shared_artifact_handler_enabled' do
    specify 'should accept boolean value' do
      expect {described_class.new(:name => 'server_local', :non_shared_artifacts_enabled => 'true')}.to_not raise_error
    end

    specify 'should not accept non-boolean value' do
      expect {described_class.new(:name => 'server_local', :non_shared_artifacts_enabled => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'artifact_storage_location' do
    specify 'should take artifact_storage_location for agent local artifact handler' do
      expect {described_class.new(:name => 'agent_local', :artifact_storage_location => '/data')}.to_not raise_error
    end

    specify 'should not artifact_storage_location for server local artifact handler' do
      expect {described_class.new(:name => 'server_local', :artifact_storage_location => '/data')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'access_key_id' do
    specify 'should take access_key_id for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :access_key_id => 'abcd')}.to_not raise_error
    end

    specify 'should not accept access_key_id for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :access_key_id => 'abcd')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'secret_access_key' do
    specify 'should take secret_access_key for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :secret_access_key => 'abcd')}.to_not raise_error
    end

    specify 'should not accept secret_access_key for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :secret_access_key => 'data')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'secret_access_key' do
    specify 'should take secret_access_key for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :secret_access_key => 'abcd')}.to_not raise_error
    end

    specify 'should not accept secret_access_key for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :secret_access_key => 'data')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'bucket_name' do
    specify 'should take bucket_name for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :bucket_name => 'abcd')}.to_not raise_error
    end

    specify 'should not accept bucket_name for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :bucket_name => 'data')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'region' do
    specify 'should take region for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :region => 'us-east-1')}.to_not raise_error
    end

    specify 'should not accept region for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :region => 'us-east-1')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'bucket_path' do
    specify 'should take bucket_path for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :bucket_path => 'abcd')}.to_not raise_error
    end

    specify 'should not accept bucket_path for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :bucket_path => 'data')}.to raise_error(Puppet::ResourceError)
    end
  end

  describe 'max_artifact_file_count' do
    specify 'should take max_artifact_file_count for s3 artifact handler' do
      expect {described_class.new(:name => 's3', :max_artifact_file_count => 11)}.to_not raise_error
    end

    specify 'should not accept non numeric value' do
      expect {described_class.new(:name => 's3', :max_artifact_file_count => "abc")}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept negative value' do
      expect {described_class.new(:name => 's3', :max_artifact_file_count => -1)}.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept max_artifact_file_count for artifact handler other than s3' do
      expect {described_class.new(:name => 'server_local', :max_artifact_file_count => 11)}.to raise_error(Puppet::ResourceError)
    end
  end
end
