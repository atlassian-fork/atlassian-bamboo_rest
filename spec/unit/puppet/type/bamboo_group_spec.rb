require 'spec_helper'

describe Puppet::Type.type(:bamboo_group) do

  describe '#name' do
    specify 'should accept alphaumberic value' do
      expect { described_class.new(:name => 'abcd1234') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => '') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept name in upper case' do
      expect { described_class.new(:name => 'abcA') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept name with \\' do
      expect { described_class.new(:name => 'abc\\') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#permissions' do
    specify 'should accept valid permission' do
      expect {described_class.new(:name => 'abcd', :permissions => [ 'ACCESS', 'CREATE_PLAN', 'CREATE_REPOSITORY', 'SOX_COMPLIANCE','RESTRICTED_ADMIN', 'ADMIN' ])}.to_not raise_error
    end

    specify 'should not accept invalid permission' do
      expect{described_class.new(:name => 'abcd', :permissions => [ 'CREATE_PLAN', 'READS' ])}.to raise_error(Puppet::ResourceError)
    end

    specify 'should compare equality correctly' do
      expect(described_class.new(:name => 'abcd', :permissions => [ 'CREATE_PLAN', 'ACCESS' ]).property(:permissions).insync?(['CREATE_PLAN', 'ACCESS'])).to eq true
    end

    specify 'should compare equality correctly for arrays in different order' do
      expect(described_class.new(:name => 'abcd', :permissions => [ 'CREATE_PLAN', 'ACCESS' ]).property(:permissions).insync?(['ACCESS', 'CREATE_PLAN'])).to eq(true)
    end

    specify 'should compare inequality correctly' do
      expect(described_class.new(:name => 'abcd', :permissions => [ 'CREATE_PLAN', 'ACCESS', 'ADMIN' ]).property(:permissions).insync?(['ACCESS', 'CREATE_PLAN'])).to eq(false)
    end
  end
end
