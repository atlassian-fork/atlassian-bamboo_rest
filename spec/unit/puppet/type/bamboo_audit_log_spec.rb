require 'spec_helper'

describe Puppet::Type.type(:bamboo_audit_log) do

  describe '#name' do
    specify 'should accept current' do
      expect { described_class.new(:name => 'current', :audit_log_enabled => :true)}.to_not raise_error
    end

    specify 'should not accept values other than current' do
      expect { described_class.new(:name => 'not_current', :audit_log_enabled => :true)}.to raise_error(Puppet::ResourceError)
    end

  end

  describe '#audit_log_enabled' do
    specify 'should accept true' do
      expect {described_class.new(:name => 'current', :audit_log_enabled => :true)}.to_not raise_error
    end

    specify 'should accept false' do
      expect {described_class.new(:name => 'current', :audit_log_enabled => :false)}.to_not raise_error
    end

    specify 'should not accept non boolean values' do
      expect {described_class.new(:name => 'current', :audit_log_enabled => 'test')}.to raise_error(Puppet::ResourceError)
    end
  end
end
