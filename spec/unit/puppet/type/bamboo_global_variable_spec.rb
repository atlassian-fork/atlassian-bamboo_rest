require 'spec_helper'

describe Puppet::Type.type(:bamboo_global_variable) do
  describe '#name' do
    specify 'should accept alphaumberic value' do
      expect { described_class.new(:name => 'abcd1234', :value => 'abcd') }.to_not raise_error
    end

    specify 'should not accept empty string' do
      expect { described_class.new(:name => '', :value => 'abcd') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept special characters' do
      expect { described_class.new(:name => 'abc>', :value => 'abcd') }.to raise_error(Puppet::ResourceError)
    end

    specify 'should not accept string longer than 255' do
      expect { described_class.new(:name => 'a'*256, :value => 'abcd') }.to raise_error(Puppet::ResourceError)
    end
  end

  describe '#value' do
    specify 'should accept any non-empty string' do
      expect { described_class.new(:name => 'abcd', :value => 'abcd') }.to_not raise_error
    end

    specify 'should accept empty string' do
      expect { described_class.new(:name => 'abcd', :value => '') }.to_not raise_error
    end

    specify 'should compare normal value equality correctly' do
      expect( described_class.new(:name => 'foo', :value => 'bar').property(:value).insync?('bar')).to eq true
    end

    specify 'should compare normal value unequality correctly' do
      expect( described_class.new(:name => 'foo', :value => 'bar').property(:value).insync?('barrrrrrrr')).to eq false
    end

    specify 'should compare password value equality correctly' do
      expect( described_class.new(:name => 'foopassword', :value => 'bar').property(:value).insync?('{PKCS5S2}C6ze5CncN0LyuY4coLmCcDTUPHdEF3dxTSQT/YEpJ5ygt+0bzWHnDePnijognxgn')).to eq true
    end

    specify 'should compare password value unequality correctly' do
      expect( described_class.new(:name => 'foopassword', :value => 'bar').property(:value).insync?('{PKCS5S2}abcdsssdfsdifksifksifksifksjfiafj')).to eq false
    end

    specify 'should log actual new value change for non password variable' do
      expect( described_class.new(:name => 'foo', :value => 'bar').property(:value).should_to_s('bar') ).to eq 'bar'
    end

    specify 'should not log actual new value change for password variable' do
      expect( described_class.new(:name => 'foopassword', :value => 'bar').property(:value).should_to_s('bar') ).to eq '[new password variable hash redacted]'
    end

    specify 'should log actual original value change for non password variable' do
      expect( described_class.new(:name => 'foo', :value => 'bar').property(:value).is_to_s('bar')).to eq 'bar'
    end

    specify 'should not log actual original value change for password variable' do
      expect( described_class.new(:name => 'foopassword', :value => 'bar').property(:value).is_to_s('bar') ).to eq '[old password variable hash redacted]'
    end

    specify 'should log actual value change for non password variable' do
      expect( described_class.new(:name => 'foo', :value => 'bar').property(:value).change_to_s('bar', 'foobar') ).to match /foobar/
    end

    specify 'should not log actual value change for password variable' do
      expect( described_class.new(:name => 'foopassword', :value => 'bar').property(:value).change_to_s('bar', 'foobar') ).to eq('The password variable has been changed')
    end

  end
end
