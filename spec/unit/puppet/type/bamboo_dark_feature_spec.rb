require 'spec_helper'

describe Puppet::Type.type(:bamboo_dark_feature) do
  describe '#name' do
    specify 'should accept value "bamboo.example.dark.feature"' do
      expect { described_class.new(:name => "bamboo.example.dark.feature")}.to_not raise_error
    end

    specify 'should not accept invalid value' do
      expect { described_class.new(:name => '')}.to raise_error(Puppet::ResourceError)
    end
  end

end
