require 'spec_helper'

describe Puppet::Type.type(:bamboo_elastic_configuration).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do

    let(:bamboo_elastic_configuration) do
      {
        'enabled' => true,
        'accessKeyId' => 'ABC',
        'secretAccessKey' => '123',
        'region' => 'US_EAST_1',
        'privateKeyFile' => '/etc/pkey.file',
        'certificateFile' => '/etc/cert.file',
        'uploadAwsAccountIdentifierToElasticInstances' => true,
        'maxNumOfElasticInstances' => 100,
        'allocatePublicIpToVpcInstances' => true,
        'elasticInstanceManagement' => {
          'type' => 'Custom',
          'idleAgentShutdownDelayMinutes' => 5,
          'allowedNonBambooInstances' => 20,
          'maxNumOfInstancesStart' => 30,
          'numOfBuildsInQueue' => 40,
          'numOfElasticBuildsInQueue' => 50,
          'avgQueueTimeMinutes' => 60
        },
        'elasticAutoTermination' => {
          'enabled' => true,
          'shutdownDelay' => 300
        }
      }
    end

    specify "should be able to map REST response correctly" do
      resource_hash = described_class::map_config_to_resource_hash(bamboo_elastic_configuration)
      expect(resource_hash[:enabled]).to eq :true
      expect(resource_hash[:access_key]).to eq 'ABC'
      expect(resource_hash[:secret_key]).to eq '123'
      expect(resource_hash[:region]).to eq 'US_EAST_1'
      expect(resource_hash[:private_key_file]).to eq '/etc/pkey.file'
      expect(resource_hash[:certificate_file]).to eq '/etc/cert.file'
      expect(resource_hash[:upload_aws_identifier]).to eq :true
      expect(resource_hash[:max_elastic_instances]).to eq 100
      expect(resource_hash[:allocate_public_ip_to_vpc_instances]).to eq :true

      expect(resource_hash[:im_type]).to eq 'Custom'
      expect(resource_hash[:im_idle_shutdown_delay_minutes]).to eq 5
      expect(resource_hash[:im_allowed_non_bamboo_instances]).to eq 20
      expect(resource_hash[:im_max_num_to_start]).to eq 30
      expect(resource_hash[:im_queued_build_threshold]).to eq 40
      expect(resource_hash[:im_elastic_queued_build_threshold]).to eq 50
      expect(resource_hash[:im_average_queue_time]).to eq 60

      expect(resource_hash[:termination_enabled]).to eq :true
      expect(resource_hash[:termination_shutdown_delay]).to eq 300
    end

    let(:bamboo_elastic_configuration_partial) do
      {
        'enabled' => true,
        'allocatePublicIpToVpcInstances' => false,
        'elasticAutoTermination' => {
          'enabled' => true,
          'shutdownDelay' => 400
        }
      }
    end

    specify "should be able to map partial REST response correctly" do
      resource_hash = described_class::map_config_to_resource_hash(bamboo_elastic_configuration_partial)
      expect(resource_hash[:enabled]).to eq :true
      expect(resource_hash[:allocate_public_ip_to_vpc_instances]).to eq :false
      expect(resource_hash[:termination_enabled]).to eq :true
      expect(resource_hash[:termination_shutdown_delay]).to eq 400
      expect(resource_hash[:region]).to eq nil
    end

    let(:bamboo_elastic_configuration_agro) do
      {
        'elasticInstanceManagement' => {
          'type' => 'Aggresive',
          'idleAgentShutdownDelayMinutes' => 30
        }
      }
    end

    specify "should only return instance management settings when type == Custom" do
      resource_hash = described_class::map_config_to_resource_hash(bamboo_elastic_configuration_agro)
      expect(resource_hash[:im_type]).to eq 'Aggresive'
      expect(resource_hash[:im_idle_shutdown_delay_minutes]).to eq nil
    end

  end

  describe '#map_resource_to_config' do
    let (:instance) do
      instance = described_class.new()
      resource = {
        :enabled                             => :true,
        :access_key                          => 'ABC',
        :secret_key                          => '123',
        :region                              => 'US_EAST_1',
        :private_key_file                    => '/etc/random/pkey.file',
        :certificate_file                    => '/etc/random/certificate.file',
        :upload_aws_identifier               => :true,
        :max_elastic_instances               => 100,
        :allocate_public_ip_to_vpc_instances => :true,

        :im_type                             => 'Custom',
        :im_idle_shutdown_delay_minutes      => 5,
        :im_allowed_non_bamboo_instances     => 6,
        :im_max_num_to_start                 => 10,
        :im_queued_build_threshold           => 15,
        :im_elastic_queued_build_threshold   => 20,
        :im_average_queue_time               => 30,

        :termination_enabled                 => :true,
        :termination_shutdown_delay          => 300
      }

      instance.resource = resource
      instance
    end

    let (:bamboo_elastic_configuration) do
      instance.map_resource_hash_to_config
    end

    specify { expect(bamboo_elastic_configuration['enabled']).to eq :true }
    specify { expect(bamboo_elastic_configuration['accessKeyId']).to eq 'ABC' }
    specify { expect(bamboo_elastic_configuration['secretAccessKey']).to eq '123' }
    specify { expect(bamboo_elastic_configuration['region']).to eq 'US_EAST_1' }
    specify { expect(bamboo_elastic_configuration['privateKeyFile']).to eq '/etc/random/pkey.file' }
    specify { expect(bamboo_elastic_configuration['certificateFile']).to eq '/etc/random/certificate.file' }
    specify { expect(bamboo_elastic_configuration['uploadAwsAccountIdentifierToElasticInstances']).to eq :true }
    specify { expect(bamboo_elastic_configuration['maxNumOfElasticInstances']).to eq 100 }
    specify { expect(bamboo_elastic_configuration['allocatePublicIpToVpcInstances']).to eq :true }

    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['type']).to eq 'Custom' }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['idleAgentShutdownDelayMinutes']).to eq 5 }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['allowedNonBambooInstances']).to eq 6 }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['maxNumOfInstancesStart']).to eq 10 }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['numOfBuildsInQueue']).to eq 15 }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['numOfElasticBuildsInQueue']).to eq 20 }
    specify { expect(bamboo_elastic_configuration['elasticInstanceManagement']['avgQueueTimeMinutes']).to eq 30 }

    specify { expect(bamboo_elastic_configuration['elasticAutoTermination']['enabled']).to eq :true }
    specify { expect(bamboo_elastic_configuration['elasticAutoTermination']['shutdownDelay']).to eq 300 }

  end

end