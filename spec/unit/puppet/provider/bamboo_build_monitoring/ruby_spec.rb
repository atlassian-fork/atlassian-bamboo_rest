require 'spec_helper'

describe Puppet::Type.type(:bamboo_build_monitoring).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    let (:build_monitoring) do
      {
        'buildMonitoringEnabled'          => true,
        'buildTimeMultiplierDefault'      => 3.0,
        'logQuietMinutesTimeDefault'      => 2,
        'buildQueueMinutesTimeoutDefault' => 1
      }
    end

    let(:resource_hash) do
      #puts "#{resource_hash}"
      described_class::map_config_to_resource_hash(build_monitoring)
    end

    specify { expect(resource_hash[:build_monitoring_enabled]).to eq :true }
    specify { expect(resource_hash[:build_time_multiplier_default]).to eq 3.0}
    specify { expect(resource_hash[:log_quiet_minutes_time_default]).to eq 2}
    specify { expect(resource_hash[:build_queue_minutes_timeout_default]).to eq 1}

  end

  describe '#map_resource_to_config' do
    let(:instance) do
      instance = described_class.new()
      resource = {
        :build_monitoring_enabled            => :false,
        :build_time_multiplier_default       => 4.0,
        :log_quiet_minutes_time_default      => 5,
        :build_queue_minutes_timeout_default => 6,
      }
      instance.resource = resource
      instance
    end

    let(:build_monitoring) do
      instance.map_resource_hash_to_config
    end

    specify { expect(build_monitoring['buildMonitoringEnabled']).to eq :false }
    specify { expect(build_monitoring['buildTimeMultiplierDefault']).to eq 4.0 }
    specify { expect(build_monitoring['logQuietMinutesTimeDefault']).to eq 5 }
    specify { expect(build_monitoring['buildQueueMinutesTimeoutDefault']).to eq 6 }
  end
end