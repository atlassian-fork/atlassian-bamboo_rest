require 'spec_helper'

describe Puppet::Type.type(:bamboo_administration_general_configuration).provider(:ruby) do |variable|
  describe '.map_config_to_resource_hash' do
    let(:bamboo_settings) do
      {
        'instanceName'                   => 'my bamboo instance',
        'baseUrl'                        => 'http://bamboo.atlassian.com',
        'brokerUrl'                      => 'tcp://localhost',
        'brokerClientUrl'                => 'tcp://localhost',
        'dashboardDefaultPageSize'       => 50,
        'branchDetectionIntervalSeconds' => 180,
        'enableGravatarSupport'          => true,
        'gravatarServerUrl'              => 'http://gravatar.com/',
        'enableGzipCompression'          => true
      }
    end

    let(:resource_hash) do
    	described_class::map_config_to_resource_hash(bamboo_settings)
    end
    
    specify { expect(resource_hash[:instance_name]).to eq('my bamboo instance') }
    specify { expect(resource_hash[:base_url]).to eq('http://bamboo.atlassian.com') }
    specify { expect(resource_hash[:broker_url]).to eq('tcp://localhost') }
    specify { expect(resource_hash[:broker_client_url]).to eq("tcp://localhost") }
    specify { expect(resource_hash[:dashboard_page_size]).to eq(50) }
    specify { expect(resource_hash[:branch_detection_interval]).to eq(180) }
    specify { expect(resource_hash[:gravatar_enabled]).to eq(:true) }
    specify { expect(resource_hash[:gravatar_server_url]).to eq('http://gravatar.com') }
    specify { expect(resource_hash[:gzip_compression_enabled]).to eq(:true) }
  end

  describe '#map_resource_to_config' do

    let(:resource) do
      resource = {}
      resource[:instance_name] = 'my bamboo instance'
      resource[:base_url] = 'http://atlassian.com'
      resource[:broker_url] = 'tcp://localhost'
      resource[:broker_client_url] = 'tcp://localhost'
      resource[:dashboard_page_size] = 50
      resource[:branch_detection_interval] = 180
      resource[:gravatar_enabled] = true
      resource[:gravatar_server_url] = 'http://gravatar.com'
      resource[:gzip_compression_enabled] = false
      resource
    end

    let(:instance) do
      instance = described_class.new
      instance.resource = resource
      instance
    end

    let(:config) do
      instance.map_resource_hash_to_config
    end

    specify { expect(config['instanceName']).to eq('my bamboo instance')}
    specify { expect(config['baseUrl']).to eq('http://atlassian.com')}
    specify { expect(config['brokerUrl']).to eq('tcp://localhost')}
    specify { expect(config['brokerClientUrl']).to eq('tcp://localhost')}
    specify { expect(config['dashboardDefaultPageSize']).to eq(50)}
    specify { expect(config['branchDetectionIntervalSeconds']).to eq(180)}
    specify { expect(config['enableGravatarSupport']).to eq(true)}
    specify { expect(config['gravatarServerUrl']).to eq('http://gravatar.com')}
    specify { expect(config['enableGzipCompression']).to eq(false)}
  end
end