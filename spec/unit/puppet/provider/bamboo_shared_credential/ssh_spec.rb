require 'spec_helper'

describe Puppet::Type.type(:bamboo_shared_credential).provider(:ssh) do |variable|
  describe '.map_config_to_resource_hash' do
    specify 'should map json response correctly to resource hash' do
      config = {
        'results' => [
          {
            'id' => '111',
            'name' => 'sshSharedCredential',
            'attributes' => {
              'sshKey' => 'key',
              'sshPassphrase' => 'passphrase',
            }
          }
        ]
      }

      resource_hash = described_class.map_config_to_resource_hash(config)
      expect(resource_hash[0].name).to eq 'sshSharedCredential'
      expect(resource_hash[0].ssh_key).to eq 'key'
      expect(resource_hash[0].ssh_passphrase).to eq 'passphrase'
    end
  end

  describe '#map_resource_hash_to_config' do
    specify 'should fail if ssh_key is missing' do
      resource = {:name=>'cred', :ssh_passhphrase => 'passphrase'}
      provider = described_class.new
      provider.resource = resource

      expect {provider.map_resource_hash_to_config}.to raise_error(ArgumentError)
    end

    specify 'should map resource hash correctly to json' do
      resource = {:name=> 'sshCred', :ssh_key => 'key', :ssh_passphrase => 'passphrase'}
      provider = described_class.new
      provider.resource = resource

      json = provider.map_resource_hash_to_config
      expect(json['name']).to eq 'sshCred'
      expect(json['attributes']['sshKey']).to eq 'key'
      expect(json['attributes']['sshPassphrase']).to eq 'passphrase'
    end
  end
end
