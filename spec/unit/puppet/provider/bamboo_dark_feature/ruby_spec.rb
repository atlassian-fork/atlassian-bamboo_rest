require 'spec_helper'

describe Puppet::Type.type(:bamboo_dark_feature).provider(:ruby) do

  describe '.map_config_to_resource_hash' do
    let(:config) do
      [
        { 'key' => 'bamboo.final.stages' },
        { 'key' => 'bamboo.branches.initial.build.changelist' }
      ]
    end
    specify 'should map valid json config to valid resource hashes' do
      resources = described_class.map_config_to_resource_hash(config)
      expect (resources[0].name).equal?('bamboo.final.stages')
    end
  end

end
